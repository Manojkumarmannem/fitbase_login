package stepdefinations;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import driverFactory.Driverfactory;
import io.cucumber.java.en.*;
import pageobjects.Loginpage;
public class LoginSteps {
	Loginpage login = new Loginpage(Driverfactory.getdriver());
	WebDriver driver;
	@Given("User Launch Fitbase application")
	public void user_launch_fitbase_application() {
		Driverfactory.getdriver().get("https://qa.fitbase.com/");
		Assert.assertTrue("Fitbase - Wellness. Anywhere. Anytime.", true);
	}
	@When("User Clickon signup and login button")
	public void user_clickon_signup_and_login_button() throws InterruptedException {
		Thread.sleep(2000);
		login.SignupandLoginbutton().click();
	}
	@And("User enter Username {string}")
	public void user_enter_username(String username){
		login.Username(username);
	}
	@And("User enter password {string}")
	public void user_enter_password(String password) {
		login.Password(password);
	}
	@Then("Click on log in with email button")
	public void click_on_log_in_with_email_button() throws Exception {
		login.Loginwithemailbutton();
		Thread.sleep(5000);
	}
	@Then("after login verify the page Title")
	public void after_login_verify_the_page_title() { 
		Assert.assertEquals("Dashboard", driver.getTitle());
	}
}
