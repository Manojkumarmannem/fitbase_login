package hooks;
import java.util.Properties;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import driverFactory.Driverfactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utils.Configreader;
public class ApplicationHooks {
	public WebDriver driver;	
	Properties prop;
	Driverfactory driverfactory;
	Configreader configreader;
	@Before(order = 0)
	public void getProperty() throws Exception {
		configreader = new Configreader();
		prop = configreader.initprop();
	}
	@Before(order = 1)
	public void launchBrowser() {
		String browsername = prop.getProperty("browser");
		driverfactory = new Driverfactory();
		driver =driverfactory.init_driver(browsername);
	}
	@After(order = 1)
	public void teardown(Scenario scenario) {	
		String sceenshot = scenario.getName().replaceAll("", "_");
		byte[] source=  ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
		scenario.attach(source, "image/png", sceenshot);
	}
	//	@After(order=0)
	//	public void quit() {
	//		driver.quit();
	//	}	
}
