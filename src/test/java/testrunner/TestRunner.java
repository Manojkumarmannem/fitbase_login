package testrunner;
import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/Feature/Login.feature",
		glue = {"stepdefinations","hooks" },
		plugin = { "pretty", "html:target/html", "json:target/cucumber.json"}, 		
		dryRun = false,
		monochrome = true)
public class TestRunner {
}
