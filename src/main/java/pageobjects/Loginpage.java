package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Loginpage {
	public WebDriver driver;
	public Loginpage(WebDriver driver) {
		this.driver=driver;
	}
	By SignupandLoginbutton = By.xpath("//*[@id=\'signuplogin\']/a");
	By Username             = By.xpath("//*[@id=\'username\']");
	By Password             = By.xpath("//*[@id=\'password\']");
	By Loginwithemailbutton = By.xpath("//*[@id= \'loginemail\']");

	public WebElement  SignupandLoginbutton() {
		return driver.findElement(SignupandLoginbutton);}
	public void Username(String username) {
		driver.findElement(Username).sendKeys(username);}
	public void Password(String password) {
		driver.findElement(Password).sendKeys(password);}
	public void Loginwithemailbutton() {
		driver.findElement(Loginwithemailbutton).click();}
}




