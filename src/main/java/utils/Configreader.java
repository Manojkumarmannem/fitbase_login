package utils;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
public class Configreader {
	Properties prop;
	public Properties initprop() throws Exception {
		prop=new Properties();
		FileInputStream input;
		try {
			input= new FileInputStream("./src/test/resources/Config/Configuration.properties");
			prop.load(input);
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return prop;
	}
}



